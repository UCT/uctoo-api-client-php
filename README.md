uctoo-api-client-php 是一个轻量级、语义化、对IDE友好的HTTP客户端，支持常见的HTTP请求、异步请求和并发请求，是PHP开发者向UCToo服务端发送请求的SDK工具。

> uctoo-api-client-php 并不强制依赖于cURL，如果没有安装cURL，uctoo-api-client-php会自动选择使用PHP流处理，或者你也可以提供自己的发送HTTP请求的处理方式。

## 安装说明

#### 环境依赖

- PHP >= 5.5.0
- 如果使用PHP流处理，allow_url_fopen 必须在php.ini中启用。
- 如果使用cURL处理，cURL >= 7.19.4，并且编译了OpenSSL 与 zlib。

#### 一键安装

    composer require uctoo/uctoo-api-client-php:dev-master

## 发起请求

#### 同步请求

###### 常规请求

```php
$response = Http::get('https://serv.uctoo.com/log/login');

$response = Http::get('https://serv.uctoo.com/log/login?name=admin');

$response = Http::get('https://serv.uctoo.com/log/login?name=admin', ['developer' => 1]);

$response = Http::post('https://serv.uctoo.com/login');

$response = Http::post('https://serv.uctoo.com/login', ['email' => 'contact@uctoo.com']);

$response = Http::patch(...);

$response = Http::put(...);

$response = Http::delete(...);

$response = Http::head(...);

$response = Http::options(...);
```

###### 指定服务端base_url的请求

```php
// 指定服务端base_url地址,最终请求地址为 https://serv.uctoo.com/login
$response = Http::withHost('https://serv.uctoo.com')->post('/login');

```

###### 指定服务端产品地址的请求

```php
// 指定服务端wechatopen产品地址,最终请求地址为 https://serv.uctoo.com/api/wechatopen/product/category/get
$response = Http::withHost('https://serv.uctoo.com')->product('/api/wechatopen')->post('/product/category/get', ['appid' => 'xxx','f_cat_id'=>0]);

```

###### 发送 Content-Type 编码请求

```php
// application/x-www-form-urlencoded(默认)
$response = Http::asForm()->post(...);

// application/json
$response = Http::asJson()->post(...);
```

###### 发送 Multipart 表单请求

```php
$response = Http::asMultipart(
    'file_input_name', file_get_contents('photo1.jpg'), 'photo2.jpg'
)->post('http://test.com/attachments');

$response = Http::asMultipart(
    'file_input_name', fopen('photo1.jpg', 'r'), 'photo2.jpg'
)->post(...);

$response = Http::attach(
    'file_input_name', file_get_contents('photo1.jpg'), 'photo2.jpg'
)->post(...);

$response = Http::attach(
    'file_input_name', fopen('photo1.jpg', 'r'), 'photo2.jpg'
)->post(...);
```
> 表单enctype属性需要设置成 multipart/form-data

###### 携带请求头的请求

```php
$response = Http::withHeaders([
    'x-powered-by' => 'uctoo'
])->post(...);
```

###### 携带重定向的请求

```php
// 默认
$response = Http::withRedirect(false)->post(...);

$response = Http::withRedirect([
    'max'             => 5,
    'strict'          => false,
    'referer'         => true,
    'protocols'       => ['http', 'https'],
    'track_redirects' => false
])->post(...);
```

###### 携带认证的请求

```php
// Basic认证
$response = Http::withBasicAuth('username', 'password')->post(...);

// Digest认证(需要被HTTP服务器支持)
$response = Http::withDigestAuth('username', 'password')->post(...);

// 服务端注册帐号认证(需要被服务器支持)
$response = Http::withAccountAuth('username', 'password')->post(...);
```

###### 携带 User-Agent 的请求
```php
$response = Http::withUA('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3100.0 Safari/537.36')->post(...);
```

###### 携带Token令牌的请求

```php
$response = Http::withToken('token')->post(...);
```

###### 携带认证文件的请求

```php
$response = Http::withCert('/path/server.pem', 'password')->post(...);
```

###### 携带SSL证书的请求

```php
// 默认
$response = Http::withVerify(false)->post(...);

$response = Http::withVerify('/path/to/cert.pem')->post(...);
```

###### 携带COOKIE的请求

```php
$response = Http::withCookies(array $cookies, string $domain)->post(...);
```

###### 携带协议版本的请求

```php
$response = Http::withVersion(1.1)->post(...);
```

###### 携带代理的请求

```php
$response = Http::withProxy('tcp://localhost:8125')->post(...);

$response = Http::withProxy([
    'http'  => 'tcp://localhost:8125', // Use this proxy with "http"
    'https' => 'tcp://localhost:9124', // Use this proxy with "https",
    'no'    => ['.cn', 'uctoo.com'] // Don't use a proxy with these
])->post(...);
```

###### 设置超时时间(单位秒)

```php
$response = Http::timeout(60)->post(...);
```

###### 设置延迟时间(单位秒)

```php
$response = Http::delay(60)->post(...);
```

###### 设置并发次数

```php
$response = Http::concurrency(10)->promise(...);
```

#### 异步请求

```php
use uctoo\uctoocloud\client\Response;
use uctoo\uctoocloud\client\RequestException;

Http::getAsync('https://serv.uctoo.com/api/wechatopen/product/category/get', ['token' => TOKEN], function (Response $response) {
    echo '异步请求成功，响应内容：' . $response->body() . PHP_EOL;
}, function (RequestException $e) {
    echo '异步请求异常，错误码：' . $e->getCode() . '，错误信息：' . $e->getMessage() . PHP_EOL;
});
echo json_encode(['code' => 200, 'msg' => '请求成功'], JSON_UNESCAPED_UNICODE) . PHP_EOL;

//输出
{"code":200,"msg":"请求成功"}
异步请求成功，响应内容：{"code":200,"msg":"success","second":3}

Http::getAsync('https://serv.uctoo.com/api/wechatopen/product/category/get', function (Response $response) {
    echo '异步请求成功，响应内容：' . $response->body() . PHP_EOL;
}, function (RequestException $e) {
    echo '异步请求异常，错误信息：' . $e->getMessage() . PHP_EOL;
});
echo json_encode(['code' => 200, 'msg' => '请求成功'], JSON_UNESCAPED_UNICODE) . PHP_EOL;

//输出
{"code":200,"msg":"请求成功"}
异步请求异常，错误信息：cURL error 1: Protocol "http1" not supported or disabled in libcurl (see https://curl.haxx.se/libcurl/c/libcurl-errors.html)

Http::postAsync(...);

Http::patchAsync(...);

Http::putAsync(...);

Http::deleteAsync(...);

Http::headAsync(...);

Http::optionsAsync(...);
```

#### 异步并发请求

```php
use uctoo\uctoocloud\client\Response;
use uctoo\uctoocloud\client\RequestException;

$promises = [
    Http::getAsync('https://serv.uctoo.com/api/wechatopen'),
    Http::getAsync('https://serv.uctoo.com/api/wechatopen/product/category/get', ['appid' => 'xxx']),
    Http::postAsync('https://serv.uctoo.com/api/wechatopen/product/brand/get', ['appid' => 'xxx']),
];

Http::concurrency(10)->multiAsync($promises, function (Response $response, $index) {
    echo "发起第 $index 个异步请求，请求时长：" . $response->json()->second . '秒' . PHP_EOL;
}, function (RequestException $e, $index) {
    echo "发起第 $index 个请求失败，失败原因：" . $e->getMessage() . PHP_EOL;
});

//输出
发起第 1 个请求失败，失败原因：cURL error 1: Protocol "http1" not supported or disabled in libcurl (see https://curl.haxx.se/libcurl/c/libcurl-errors.html)
发起第 2 个异步请求，请求时长：2 秒
发起第 0 个异步请求，请求时长：3 秒
```
> 如果未调用concurrency()方法，并发次数默认为$promises的元素个数，$promises数组里必须是异步请求

## 使用响应

发起请求后会返回一个 uctoo\uctoocloud\client\Response $response的实例，该实例提供了以下方法来检查请求的响应：

```php
$response->body() : string;
$response->json() : object;
$response->array() : array;
$response->status() : int;
$response->ok() : bool;
$response->successful() : bool;
$response->serverError() : bool;
$response->clientError() : bool;
$response->headers() : array;
$response->header($header) : string;
```

## 异常处理

请求在发生客户端或服务端错误时会抛出 uctoo\uctoocloud\client\RequestException $e异常，该实例提供了以下方法来返回异常信息：

```php
$e->getCode() : int;
$e->getMessage() : string;
$e->getFile() : string;
$e->getLine() : int;
$e->getTrace() : array;
$e->getTraceAsString() : string;
```
## 更新日志
### 2021-06-30
* 支持withHost
* 支持product产品模块路由
* 支持withAccountAuth

 **Todo List** 
 - [x] 异步请求
 - [x] 并发请求
 - [ ] 重试机制
 - [ ] 支持http2
 - [ ] 支持OAUTH2
 - [ ] 支持openapi机制，可从服务端获取API信息，客户端即可快捷使用