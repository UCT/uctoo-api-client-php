<?php

namespace uctoo\uctoocloud\client;

/**
 * @method static \uctoo\uctoocloud\client\Request asJson()
 * @method static \uctoo\uctoocloud\client\Request asForm()
 * @method static \uctoo\uctoocloud\client\Request asMultipart(string $name, string $contents, string|null $filename = null, array $headers)
 * @method static \uctoo\uctoocloud\client\Request attach(string $name, string $contents, string|null $filename = null, array $headers)
 *
 * @method static \uctoo\uctoocloud\client\Request withHost(bool|string $verify)
 * @method static \uctoo\uctoocloud\client\Request withRedirect(bool|array $redirect)
 * @method static \uctoo\uctoocloud\client\Request withStream(bool $boolean)
 * @method static \uctoo\uctoocloud\client\Request withVerify(bool|string $verify)
 * @method static \uctoo\uctoocloud\client\Request withHeaders(array $headers)
 * @method static \uctoo\uctoocloud\client\Request withBasicAuth(string $username, string $password)
 * @method static \uctoo\uctoocloud\client\Request withDigestAuth(string $username, string $password)
 * @method static \uctoo\uctoocloud\client\Request withAccountAuth(string $username, string $password)
 * @method static \uctoo\uctoocloud\client\Request withUA(string $ua)
 * @method static \uctoo\uctoocloud\client\Request withToken(string $token, string $type = 'Bearer')
 * @method static \uctoo\uctoocloud\client\Request withCookies(array $cookies, string $domain)
 * @method static \uctoo\uctoocloud\client\Request withProxy(string|array $proxy)
 * @method static \uctoo\uctoocloud\client\Request withVersion(string $version)
 * @method static \uctoo\uctoocloud\client\Request withOptions(array $options)
 *
 * @method static \uctoo\uctoocloud\client\Request product(bool|string $producy)
 *
 * @method static \uctoo\uctoocloud\client\Request delay(int $seconds)
 * @method static \uctoo\uctoocloud\client\Request timeout(int $seconds)
 * @method static \uctoo\uctoocloud\client\Request concurrency(int $times)
 *
 * @method static \uctoo\uctoocloud\client\Request get(string $url, array $query = [])
 * @method static \uctoo\uctoocloud\client\Request post(string $url, array $data = [])
 * @method static \uctoo\uctoocloud\client\Request patch(string $url, array $data = [])
 * @method static \uctoo\uctoocloud\client\Request put(string $url, array $data = [])
 * @method static \uctoo\uctoocloud\client\Request delete(string $url, array $data = [])
 * @method static \uctoo\uctoocloud\client\Request head(string $url, array $data = [])
 * @method static \uctoo\uctoocloud\client\Request options(string $url, array $data = [])
 *
 * @method static \uctoo\uctoocloud\client\Request getAsync(string $url, array|null $query = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request postAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request patchAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request putAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request deleteAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request headAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request optionsAsync(string $url, array|null $data = null, callable $success = null, callable $fail = null)
 * @method static \uctoo\uctoocloud\client\Request multiAsync(array $promises, callable $success = null, callable $fail = null)
 */

class Http extends Facade
{
    protected $facade = Request::class;
}
